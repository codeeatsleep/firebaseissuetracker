package com.example.firebaseissuetracker.service

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.firebaseissuetracker.FirebaseIssueRepository

class DownloadIssueAndCommentWorkManager(context: Context,param: WorkerParameters) : CoroutineWorker(context,param){

    override suspend fun doWork(): Result {
        val issueRepo = FirebaseIssueRepository()
        val isSuccess = issueRepo.fetchAndStoreIssueAndComment()
        if (isSuccess) {
            return Result.success()
        }
        return Result.failure()
    }
}