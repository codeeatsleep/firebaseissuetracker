package com.example.firebaseissuetracker.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = [IssueEntity::class, CommentEntity::class], version = 1, exportSchema = false)
public abstract class FireBaseIssueDatabase : RoomDatabase() {

    abstract fun issueDao(): IssueDao
    abstract fun commentDao() :CommentDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: FireBaseIssueDatabase? = null

        fun getDatabase(context: Context): FireBaseIssueDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FireBaseIssueDatabase::class.java,
                    "firebase_ios_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}