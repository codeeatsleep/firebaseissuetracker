package com.example.firebaseissuetracker.database

import androidx.appcompat.widget.DialogTitle
import androidx.room.Entity

@Entity(tableName = "Issue")
class IssueEntity : BaseEntity{
     var title :String?
     var body :String?
     var commentUrl:String?

    constructor(id: Long, updatedOn: String?, title: String?, body: String?, commentUrl: String) : super(
        id,
        updatedOn
    ) {
        this.title = title
        this.body = body
        this.commentUrl = commentUrl
    }
}