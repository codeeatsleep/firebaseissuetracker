package com.example.firebaseissuetracker.database

import androidx.room.Entity

@Entity(tableName = "Comment")
class CommentEntity : BaseEntity {
      var body :String?
      var userName :String?
      var issueId : Long

    constructor(id: Long,
                updatedOn: String?,
                body:String?,
                userName :String?,
                issueId :Long
                ) : super(id, updatedOn) {
            this.body = body
            this.userName = userName
            this.issueId = issueId
    }

}