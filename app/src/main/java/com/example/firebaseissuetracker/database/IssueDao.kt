package com.example.firebaseissuetracker.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class IssueDao : BaseDao<IssueEntity>() {
    override fun getTableName(): String {
        return "Issue"
    }
    @Query("Select * from Issue")
    abstract fun getAllIssueList() : LiveData<List<IssueEntity>>
}