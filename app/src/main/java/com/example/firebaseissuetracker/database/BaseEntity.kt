package com.example.firebaseissuetracker.database

import androidx.room.PrimaryKey

/** base class having common field across all entity model**/
open class BaseEntity {
    @PrimaryKey
    var id: Long = 0
    var updatedOn: String? = null

    constructor(id: Long, updatedOn: String?) {
        this.id = id
        this.updatedOn = updatedOn
    }

    constructor()
}