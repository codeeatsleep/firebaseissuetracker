package com.example.firebaseissuetracker.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class CommentDao  : BaseDao<CommentEntity>() {
    override fun getTableName(): String {
        return "Comment"
    }
    @Query("Select * from Comment where issueId =:issueId")
    abstract fun getAllCommentList(issueId:Long) : LiveData<List<CommentEntity>>

}