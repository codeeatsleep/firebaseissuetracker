package com.example.firebaseissuetracker.database

import androidx.room.*

/**Base Dao for all common method based on Base Model like insert/update/delete/select **/
@Dao
abstract class BaseDao<T:BaseEntity> {

    abstract fun getTableName() :String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(listOfObject: List<T>): LongArray

}