package com.example.firebaseissuetracker

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import com.example.firebaseissuetracker.api.CommentApi
import com.example.firebaseissuetracker.api.RestClient
import com.example.firebaseissuetracker.database.CommentEntity
import com.example.firebaseissuetracker.database.FireBaseIssueDatabase
import com.example.firebaseissuetracker.database.IssueEntity
import com.example.firebaseissuetracker.util.SORT_BY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class FirebaseIssueRepository {
    private val database = FireBaseIssueDatabase.getDatabase(FirebaseIssueApp.getAppContext())
    private val restClientService = RestClient.getApiService()

    //fun will return true if data is downloaded successfully otherwise false
    suspend fun fetchAndStoreIssueAndComment() : Boolean {
        try {
            // fetching all the issue list
            val result = restClientService.getAllIssuesAsync(SORT_BY)

            val issueList: MutableList<IssueEntity> = mutableListOf()
            val commentList: MutableList<CommentEntity> = mutableListOf()
            result.forEach {
                val issue = IssueEntity(it.id, it.updatedAt, it.title, it.body, it.commentsUrl)
                issueList.add(issue)
               // delay(100)
                //fetching list of comment for the issue
                val comments =  restClientService.getCommentsAsync(it.commentsUrl)
                comments.forEach { commentApi->
                    val comment = CommentEntity(commentApi.id, commentApi.updatedAt, commentApi.body, commentApi.user!!.login,issue.id)
                    commentList.add(comment)
                }
            }
            //delete the old db ,as it may be case issue status has been changed to resolved ,
            // and we are not getting that issue in open list but still it retain in db
            database.clearAllTables()
            // collected all the data and insert in bulk
            database.issueDao().insertAll(issueList)
            database.commentDao().insertAll(commentList)
            return true
        } catch (ex: Exception) {
            return false
        }
    }

     fun getAllIssue() : LiveData<List<IssueEntity>> {
        return database.issueDao().getAllIssueList()

    }
     fun getAllComment(issueId :Long) : LiveData<List<CommentEntity>>{
        return database.commentDao().getAllCommentList(issueId)
    }
}