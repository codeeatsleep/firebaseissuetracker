package com.example.firebaseissuetracker.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class IssueApi(

	@Json(name="pull_request")
	val pullRequest: PullRequest? = null,

	@Json(name="comments")
	val comments: Int? = null,

	@Json(name="closed_at")
	val closedAt: Any? = null,

	@Json(name="assignees")
	val assignees: List<AssigneesItem?>? = null,

	@Json(name="created_at")
	val createdAt: String? = null,

	@Json(name="title")
	val title: String? = null,

	@Json(name="body")
	val body: String? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="labels")
	val labels: List<LabelsItem?>? = null,

	@Json(name="labels_url")
	val labelsUrl: String? = null,

	@Json(name="author_association")
	val authorAssociation: String? = null,

	@Json(name="number")
	val number: Int? = null,

	@Json(name="milestone")
	val milestone: Any? = null,

	@Json(name="updated_at")
	val updatedAt: String? = null,

	@Json(name="events_url")
	val eventsUrl: String? = null,

	@Json(name="html_url")
	val htmlUrl: String? = null,

	@Json(name="comments_url")
	val commentsUrl: String,

	@Json(name="repository_url")
	val repositoryUrl: String? = null,

	@Json(name="id")
	val id: Long,

	@Json(name="state")
	val state: String? = null,

	@Json(name="assignee")
	val assignee: Assignee? = null,

	@Json(name="locked")
	val locked: Boolean? = null,

	@Json(name="user")
	val user: User? = null,

	@Json(name="node_id")
	val nodeId: String? = null
)

data class User(

	@Json(name="gists_url")
	val gistsUrl: String? = null,

	@Json(name="repos_url")
	val reposUrl: String? = null,

	@Json(name="following_url")
	val followingUrl: String? = null,

	@Json(name="starred_url")
	val starredUrl: String? = null,

	@Json(name="login")
	val login: String? = null,

	@Json(name="followers_url")
	val followersUrl: String? = null,

	@Json(name="type")
	val type: String? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="subscriptions_url")
	val subscriptionsUrl: String? = null,

	@Json(name="received_events_url")
	val receivedEventsUrl: String? = null,

	@Json(name="avatar_url")
	val avatarUrl: String? = null,

	@Json(name="events_url")
	val eventsUrl: String? = null,

	@Json(name="html_url")
	val htmlUrl: String? = null,

	@Json(name="site_admin")
	val siteAdmin: Boolean? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="gravatar_id")
	val gravatarId: String? = null,

	@Json(name="node_id")
	val nodeId: String? = null,

	@Json(name="organizations_url")
	val organizationsUrl: String? = null
)

data class LabelsItem(

	@Json(name="default")
	val jsonMemberDefault: Boolean? = null,

	@Json(name="color")
	val color: String? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="description")
	val description: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="node_id")
	val nodeId: String? = null
)

data class AssigneesItem(

	@Json(name="gists_url")
	val gistsUrl: String? = null,

	@Json(name="repos_url")
	val reposUrl: String? = null,

	@Json(name="following_url")
	val followingUrl: String? = null,

	@Json(name="starred_url")
	val starredUrl: String? = null,

	@Json(name="login")
	val login: String? = null,

	@Json(name="followers_url")
	val followersUrl: String? = null,

	@Json(name="type")
	val type: String? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="subscriptions_url")
	val subscriptionsUrl: String? = null,

	@Json(name="received_events_url")
	val receivedEventsUrl: String? = null,

	@Json(name="avatar_url")
	val avatarUrl: String? = null,

	@Json(name="events_url")
	val eventsUrl: String? = null,

	@Json(name="html_url")
	val htmlUrl: String? = null,

	@Json(name="site_admin")
	val siteAdmin: Boolean? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="gravatar_id")
	val gravatarId: String? = null,

	@Json(name="node_id")
	val nodeId: String? = null,

	@Json(name="organizations_url")
	val organizationsUrl: String? = null
)

data class PullRequest(

	@Json(name="patch_url")
	val patchUrl: String? = null,

	@Json(name="html_url")
	val htmlUrl: String? = null,

	@Json(name="diff_url")
	val diffUrl: String? = null,

	@Json(name="url")
	val url: String? = null
)

data class Assignee(

	@Json(name="gists_url")
	val gistsUrl: String? = null,

	@Json(name="repos_url")
	val reposUrl: String? = null,

	@Json(name="following_url")
	val followingUrl: String? = null,

	@Json(name="starred_url")
	val starredUrl: String? = null,

	@Json(name="login")
	val login: String? = null,

	@Json(name="followers_url")
	val followersUrl: String? = null,

	@Json(name="type")
	val type: String? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="subscriptions_url")
	val subscriptionsUrl: String? = null,

	@Json(name="received_events_url")
	val receivedEventsUrl: String? = null,

	@Json(name="avatar_url")
	val avatarUrl: String? = null,

	@Json(name="events_url")
	val eventsUrl: String? = null,

	@Json(name="html_url")
	val htmlUrl: String? = null,

	@Json(name="site_admin")
	val siteAdmin: Boolean? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="gravatar_id")
	val gravatarId: String? = null,

	@Json(name="node_id")
	val nodeId: String? = null,

	@Json(name="organizations_url")
	val organizationsUrl: String? = null
)
