package com.example.firebaseissuetracker.api

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface RestClientService {
   // suspend keyword do the the magic call the api in async by enqueue
  @GET("issues")
suspend fun getAllIssuesAsync(@Query("sort") sortBy : String) : List<IssueApi>
    @GET
    suspend fun getCommentsAsync(@Url url : String) : List<CommentApi>
}