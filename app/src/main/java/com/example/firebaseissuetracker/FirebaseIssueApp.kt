package com.example.firebaseissuetracker

import android.app.Application
import androidx.work.*
import com.example.firebaseissuetracker.service.DownloadIssueAndCommentWorkManager
import com.facebook.stetho.Stetho
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class FirebaseIssueApp :Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        Stetho.initializeWithDefaults(this)
        startDownloadingAsteroidTask()
    }

    private fun startDownloadingAsteroidTask() {
        CoroutineScope(Dispatchers.Main).launch {
            val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            val repeatWorkRequest = PeriodicWorkRequestBuilder<DownloadIssueAndCommentWorkManager>(1,
                TimeUnit.DAYS).setConstraints(constraints).build()
            WorkManager.getInstance().enqueueUniquePeriodicWork(
                "asteroid_download"  , ExistingPeriodicWorkPolicy.REPLACE,repeatWorkRequest
            )
        }
    }

    companion object {
        lateinit var instance: FirebaseIssueApp
        fun getAppContext() : FirebaseIssueApp {
            return instance
        }
    }
}