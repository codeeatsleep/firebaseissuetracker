package com.example.firebaseissuetracker.issue

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.firebaseissuetracker.MainActivity
import com.example.firebaseissuetracker.R
import kotlinx.android.synthetic.main.issue_fragment.*
import kotlinx.android.synthetic.main.issue_fragment.view.*

class IssueFragment : Fragment() {

    private lateinit var viewModel: IssueViewModel
    private lateinit var adapter: IssueRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.issue_fragment, container, false)
        (requireActivity() as MainActivity).supportActionBar?.title = getString(R.string.app_name)
        adapter = IssueRecyclerAdapter(IssueRecyclerAdapter.OnClickListener {
            val action = IssueFragmentDirections.actionShowComment(it.id)
            findNavController().navigate(action)
        })
        view.issue_list.adapter =  adapter
        val dividerItemDecoration =
          DividerItemDecoration(view.issue_list.context, DividerItemDecoration.VERTICAL)
        view.issue_list.addItemDecoration(dividerItemDecoration)
        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(IssueViewModel::class.java)
        viewModel.issueList.observe(viewLifecycleOwner, Observer {
            progressBar.visibility = View.GONE
            adapter.submitList(it)
        })
    }

}