package com.example.firebaseissuetracker.issue

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseissuetracker.database.IssueEntity
import com.example.firebaseissuetracker.databinding.IssueItemViewBinding

class IssueRecyclerAdapter(private val onClickListener: OnClickListener) : ListAdapter<IssueEntity, IssueRecyclerAdapter.IssueViewHolder>(DiffCallback) {
    companion object DiffCallback : DiffUtil.ItemCallback<IssueEntity>() {
        override fun areItemsTheSame(oldItem: IssueEntity, newItem: IssueEntity): Boolean {
            return oldItem.id == newItem.id

        }

        override fun areContentsTheSame(oldItem: IssueEntity, newItem: IssueEntity): Boolean {
            return oldItem.updatedOn == newItem.updatedOn

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueViewHolder {
        return IssueViewHolder(
            IssueItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: IssueViewHolder, position: Int) {
        val issue = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(issue)
        }
        holder.bind(issue)
    }

    class IssueViewHolder(private var binding: IssueItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(issue: IssueEntity) {
            binding.item = issue
            binding.executePendingBindings()
        }
    }

    class OnClickListener(val clickListener: (issue: IssueEntity) -> Unit) {
        fun onClick(issue: IssueEntity) = clickListener(issue)
    }
}