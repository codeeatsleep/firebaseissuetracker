package com.example.firebaseissuetracker.issue

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.firebaseissuetracker.MainActivity
import com.example.firebaseissuetracker.R
import kotlinx.android.synthetic.main.fragment_detail.view.*
import kotlinx.android.synthetic.main.issue_fragment.view.*

class CommentFragment : Fragment() {
    private lateinit var viewModel: IssueViewModel
    private lateinit var adapter: CommentRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        adapter = CommentRecyclerAdapter()
        val dividerItemDecoration =
            DividerItemDecoration(view.comment_list.context, DividerItemDecoration.VERTICAL)
        view.comment_list.addItemDecoration(dividerItemDecoration)
        view.comment_list.adapter =  adapter
        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as MainActivity).supportActionBar?.title = getString(R.string.title_comment)
        viewModel = ViewModelProvider(this).get(IssueViewModel::class.java)
        val safeArgs: CommentFragmentArgs by navArgs()
        viewModel.setIssueId(safeArgs.id)
        viewModel.commentList.observe(viewLifecycleOwner, Observer {
            if(it.isNullOrEmpty()){
                view?.no_comment_found?.visibility = View.VISIBLE
            } else {
                view?.no_comment_found?.visibility = View.GONE
            }

            adapter.submitList(it)
        })
    }

}