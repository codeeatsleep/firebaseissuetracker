package com.example.firebaseissuetracker.issue

import androidx.lifecycle.*
import com.example.firebaseissuetracker.FirebaseIssueRepository
import com.example.firebaseissuetracker.database.IssueEntity
import kotlinx.coroutines.launch

class IssueViewModel : ViewModel() {
    private val issueRepo = FirebaseIssueRepository()
    private val currentSelectIssueId = MutableLiveData<Long>()

    val issueList: LiveData<List<IssueEntity>> by lazy {
        issueRepo.getAllIssue()
    }

    val commentList  = Transformations.switchMap(currentSelectIssueId) {
        issueRepo.getAllComment(it)
    }

    fun setIssueId(issueId: Long) {
        if (issueId != currentSelectIssueId.value) {
            currentSelectIssueId.value = issueId
        }
    }
}