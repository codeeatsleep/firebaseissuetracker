package com.example.firebaseissuetracker.issue

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseissuetracker.database.CommentEntity
import com.example.firebaseissuetracker.databinding.CommentItemViewBinding

class CommentRecyclerAdapter() : ListAdapter<CommentEntity, CommentRecyclerAdapter.CommentViewHolder>(DiffCallback) {
    companion object DiffCallback : DiffUtil.ItemCallback<CommentEntity>() {
        override fun areItemsTheSame(oldItem: CommentEntity, newItem: CommentEntity): Boolean {
            return oldItem.id == newItem.id

        }

        override fun areContentsTheSame(oldItem: CommentEntity, newItem: CommentEntity): Boolean {
            return oldItem.updatedOn == newItem.updatedOn

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(
            CommentItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val comment = getItem(position)
        holder.bind(comment)
    }

    class CommentViewHolder(private var binding: CommentItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: CommentEntity) {
            binding.item = comment
            binding.executePendingBindings()
        }
    }

    class OnClickListener(val clickListener: (comment: CommentEntity) -> Unit) {
        fun onClick(comment: CommentEntity) = clickListener(comment)
    }
}