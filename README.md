# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Problem Statement ###
§  Display a list of issues per user

§  Order by most-recently updated issue first

§  Issue titles and first 140 characters of the issue body should be shown in the list

§  Allow the user to tap an issue to display next screen (detail screen) containing all comments for that issue.

§  All the comments should be shown on the detail screen. The complete comment body and username of each comment author should be shown on this screen.

§  Implement persistent storage in the application for caching data so that the issues are only fetched once in 24 hours. The persistent storage should only contain the latest data and any old data can be discarded.

### My Approach ###
as we need to fetch once a day, i just need a work manager which fetch data once a day and update into database(if data is same as before discard it)

### Arch. ###
1. user Work Manager to download data in background(as it internally handle different OS)
2. follow  MVVM arch.
3. use lifecycle aware component # live data,viewModel for smooth functioning of app
4. Use Kotlin coroutine for asyn task
5. use retrofit for network communication
6. data binding
7. moshi for kotlin friendly json parsing
